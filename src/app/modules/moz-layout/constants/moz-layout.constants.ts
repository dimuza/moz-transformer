export const MOZ_LAYOUT = {
    T_HEADER:"t_header",
    M_HEADER:"m_header",
    B_HEADER:"b_header",
    T_FOOTER:"t_footer",
    M_FOOTER:"m_footer",
    B_FOOTER:"b_footer",
    L_SIDEBAR:"l_sidebar",
    R_SIDEBAR:"r_sidebar",
    L_CONTENT:"l_content",
    R_CONTENT:"r_content",
};
