import {MozLayoutConfig} from "../entities/moz-layout-config";

export class MozStyleGenerator {


    constructor(public config: MozLayoutConfig) {
    }

    getStyle() {
        let config = this.config;
        let style = {};

        style['display'] = 'grid';

        style['grid-template-columns'] = [
            config.l_sidebar.value + "px",
            config.l_content.value + "px",
            'auto',
            config.r_content.value + "px",
            config.r_sidebar.value + "px"
        ].join(' ');

        style['grid-template-rows'] = [
            config.t_header.value + "px",
            config.m_header.value + "px",
            config.b_header.value + "px",
            "auto",
            config.t_footer.value + "px",
            config.m_footer.value + "px",
            config.b_footer.value + "px"
        ].join(' ');

        let gridTemplate = [
            ['t_header', 't_header', 't_header', 't_header', 't_header'].join(" "),
            ['m_header', 'm_header', 'm_header', 'm_header', 'm_header'].join(" "),
            ['b_header', 'b_header', 'b_header', 'b_header', 'b_header'].join(" "),
            ['l_sidebar', 'l_content', 'm_content', 'r_content', 'r_sidebar'].join(" "),
            ['t_footer', 't_footer', 't_footer', 't_footer', 't_footer'].join(" "),
            ['m_footer', 'm_footer', 'm_footer', 'm_footer', 'm_footer'].join(" "),
            ['b_footer', 'b_footer', 'b_footer', 'b_footer', 'b_footer'].join(" ")
        ];

        style['grid-template-areas'] = gridTemplate.reduce(function (current, item) {
            current += "'" + item + "'";
            return current;
        }, "");
        return style;
    }

}