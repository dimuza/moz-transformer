import {Injectable} from '@angular/core';
import {MozLayoutConfig} from "../entities/moz-layout-config";
import {MozStyleGenerator} from "../generator/moz-style.generator";
import {MozLayoutController} from "../controlers/moz-layout.controller";
import {Sine,Power4,Elastic,Back} from "gsap";
import {L_STATES} from "../entities/moz-base-object-config";

@Injectable()
export class MozLayoutService extends MozLayoutController{

    constructor(){

        let config = new MozLayoutConfig();
        config.t_footer.max = 400;
        config.t_footer.min = 100;
        config.t_footer.states.on = 400;
        config.t_footer.states.off = 100;



        config.m_header.resizable = true;
        config.b_header.resizable = true;
        config.m_footer.resizable = true;
        config.b_footer.resizable = true;

        config.l_sidebar.max = 1000;
        config.l_sidebar.resizable = true;
        config.l_content.resizable = true;
        config.r_sidebar.resizable = true;
        config.r_content.resizable = true;

        config.t_footer.resizable = true;
        config.t_footer.states.on = 300;
        config.t_footer.animation_config.speed = 0.3;
        config.t_footer.animation_config.animation = Power4.easeIn;


        config.r_content.max = 900;
        config.r_content.states.on = 800;
        super(config);
    }

}
