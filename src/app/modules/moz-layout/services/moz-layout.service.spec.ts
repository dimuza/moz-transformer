import { TestBed, inject } from '@angular/core/testing';

import { MozLayoutService } from './moz-layout.service';

describe('MozLayoutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MozLayoutService]
    });
  });

  it('should be created', inject([MozLayoutService], (service: MozLayoutService) => {
    expect(service).toBeTruthy();
  }));
});
