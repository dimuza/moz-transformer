import {EventEmitter} from "@angular/core";
import {MozLayoutConfig} from "../entities/moz-layout-config";
import {MozStyleGenerator} from "../generator/moz-style.generator";
import {Moz_GSAP_Animation_Config, Moz_GSAP_Animations} from "../../moz-gsap/moz-gsap-animation.class";
import {MOZ_LAYOUT} from "../constants/moz-layout.constants";
import {L_STATES} from "../entities/moz-base-object-config";
import {MozLayoutConfig_Row} from "../entities/moz-row-config";
import {MozLayoutConfig_Column} from "../entities/moz-colum-config";

export class MozLayoutBaseController{
    protected ___config_change_event:EventEmitter<MozLayoutConfig> = new EventEmitter();
    protected ___config:MozLayoutConfig;
    protected ___style:any;

    constructor(config:MozLayoutConfig) {
        this.___config = config;
        this.setStyle();

    }

    protected setStyle(){
        let styleGenerator = new MozStyleGenerator(this.___config);
        this.___style = styleGenerator.getStyle();
    }

    animate(entity:string,value:number,animationConfig = new Moz_GSAP_Animation_Config()){
        MozLayoutConfig.checkEntity(entity);
        let animation = new Moz_GSAP_Animations(this.___config[entity].value,value);
        animation.animate(animationConfig).subscribe((value)=>{
            this.___config[entity].value = value;
            this.setStyle();
        });
    }

    on(entity){
        let configEntity = this.getConfigEntity(entity);
        this.animate(entity,configEntity.states.on);
        configEntity.state = L_STATES.ON;
    }

    off(entity){
        let configEntity = this.getConfigEntity(entity);
        this.animate(entity,configEntity.states.off);
        configEntity.state = L_STATES.OFF;
    }

    protected getConfigEntity(entity:string):MozLayoutConfig_Row | MozLayoutConfig_Column{
        MozLayoutConfig.checkEntity(entity);
        return this.___config[entity];
    }
    getStyle() {
        return this.___style;
    }

}