import {MozLayoutConfig} from "../entities/moz-layout-config";
import {L_STATES} from "../entities/moz-base-object-config";
import {MozLayoutDragController} from "./moz-layout-drag-controller";

export class MozLayoutComponentsController extends MozLayoutDragController{



    constructor(config: MozLayoutConfig) {
        super(config);
    }

    toggle(entity:string,newState:string = "",toogleStates = [L_STATES.ON,L_STATES.OFF]){
        entity = MozLayoutConfig.checkEntity(entity);
        let toggleObject  = this.getConfigEntity(entity);

        if(!newState){
            newState = toogleStates[toogleStates.indexOf(toggleObject.state) + 1]? toogleStates[toogleStates.indexOf(toggleObject.state) + 1] : toogleStates[0];
        }
        toggleObject.state = newState;
        this.animate(entity,toggleObject.states[newState],toggleObject.animation_config);
    }

    notch(entity:string){
        return this.getConfigEntity(entity).resizable;
    }

    removedGhost(){
        //Problem unnecessary ghost appear while dragging. (SOLIUTION)   https://kryogenix.org/code/browser/custom-drag-image.html
        let elements = document.getElementsByClassName("notch");
        for (let i = 0; i < elements.length; i++) {
            elements[i].addEventListener("dragstart", function(e:any) {
                let crt = this.cloneNode(true);
                crt.style.display = "none";
                document.body.appendChild(crt);
                e.dataTransfer.setDragImage(crt, 0, 0);
            }, false);
        }
    }

}