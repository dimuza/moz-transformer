import {MozLayoutBaseController} from "./moz-layout-base.controller";
import {MozLayoutConfig} from "../entities/moz-layout-config";
import {MOZ_LAYOUT} from "../constants/moz-layout.constants";
export const DRAG_STRATEGY = {
    L_TO_R : "from_left_to_right",
    R_TO_L : "from_right_to_left",
};
export class MozLayoutDragController extends MozLayoutBaseController{

    constructor(config: MozLayoutConfig) {

        super(config);

    }

    dragStart(event,entity:string){

        this.___config.dragging = true;

    }

    dragging(e, entity){
        //After release e.pageX becomes = 0; Don't save this value.
            if(e.pageX){

                    let configEntity = this.getConfigEntity(entity);

                    configEntity.value = this.dragCalculationBasedOnEntity(e,entity);

                    this.setStyle();

            }
    }

    dragEnd(event,entity){

        this.checkMinMax(entity);

        this.___config.dragging = false;

    }

    protected checkMinMax(entity){

        let configEntity = this.getConfigEntity(entity);

        if(configEntity.value > configEntity.max){

            this.animate(entity,configEntity.max,configEntity.animation_config);

        }

        if(configEntity.value < configEntity.min){

            this.off(entity);

        }
    }

    protected dragCalculationBasedOnEntity(event,entity){

        let calculation = {};

        calculation[MOZ_LAYOUT.L_SIDEBAR] = ()=>{

            return event.pageX;

        };

        calculation[MOZ_LAYOUT.L_CONTENT] = ()=>{

            return event.pageX - this.getConfigEntity(MOZ_LAYOUT.L_SIDEBAR).value;

        };

        calculation[MOZ_LAYOUT.R_SIDEBAR] = ()=>{
            return window.innerWidth - event.pageX;
        };

        calculation[MOZ_LAYOUT.R_CONTENT] = ()=>{
            return window.innerWidth - event.pageX - this.getConfigEntity(MOZ_LAYOUT.R_SIDEBAR).value;
        };

        calculation[MOZ_LAYOUT.T_FOOTER] = ()=>{
          return window.innerHeight - event.pageY - this.getConfigEntity(MOZ_LAYOUT.M_FOOTER).value - this.getConfigEntity(MOZ_LAYOUT.B_FOOTER).value;
        };

        calculation[MOZ_LAYOUT.M_FOOTER] = ()=>{
            return window.innerHeight - event.pageY  - this.getConfigEntity(MOZ_LAYOUT.B_FOOTER).value;
        };

        calculation[MOZ_LAYOUT.B_FOOTER] = ()=>{
            return window.innerHeight - event.pageY;
        };

        calculation[MOZ_LAYOUT.B_HEADER] = ()=>{
            return event.pageY - this.getConfigEntity(MOZ_LAYOUT.M_HEADER).value - this.getConfigEntity(MOZ_LAYOUT.T_HEADER).value;
        };

        calculation[MOZ_LAYOUT.B_HEADER] = ()=>{
            return event.pageY - this.getConfigEntity(MOZ_LAYOUT.M_HEADER).value - this.getConfigEntity(MOZ_LAYOUT.T_HEADER).value;
        };

        calculation[MOZ_LAYOUT.M_HEADER] = ()=>{
            return event.pageY - this.getConfigEntity(MOZ_LAYOUT.T_HEADER).value;
        };

        calculation[MOZ_LAYOUT.T_HEADER] = ()=>{
            return event.pageY;
        };

        if(calculation.hasOwnProperty(entity)){

            return calculation[entity]();

        }else {

            throw new Error("Provided entity ["+entity+"] do not exsist in calculation array");

        }


    }


}