import {MozLayoutComponentsController} from "./moz-layout-components.controller";
import {MozLayoutConfig} from "../entities/moz-layout-config";
import {MOZ_LAYOUT} from "../constants/moz-layout.constants";

export class MozLayoutController extends MozLayoutComponentsController{


    constructor(config: MozLayoutConfig) {
        super(config);
    }

    toogleLSideBar() {
        this.toggle(MOZ_LAYOUT.L_SIDEBAR);
    }
    toogleRSideBar() {
        this.toggle(MOZ_LAYOUT.R_SIDEBAR);
    }
    toogleLContent() {
        this.toggle(MOZ_LAYOUT.L_CONTENT);
    }
    toogleRContent() {
        this.toggle(MOZ_LAYOUT.R_CONTENT);
    }

    toogleTHeader() {
        this.toggle(MOZ_LAYOUT.T_HEADER);
    }
    toogleMHeader() {
        this.toggle(MOZ_LAYOUT.M_HEADER);
    }
    toogleBHeader() {
        this.toggle(MOZ_LAYOUT.B_HEADER);
    }
    toogleTFooter() {
        this.toggle(MOZ_LAYOUT.T_FOOTER);
    }
    toogleMFooter() {
        this.toggle(MOZ_LAYOUT.M_FOOTER);
    }
    toogleBFooter() {
        this.toggle(MOZ_LAYOUT.B_FOOTER);
    }
}