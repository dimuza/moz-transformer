import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MozLayoutComponent} from './moz-layout.component';
import {MozLayoutService} from "./services/moz-layout.service";
import {MozDynamicComponentModule} from "../moz-dynamic-component/moz-dynamic-component.module";

@NgModule({
    imports: [
        CommonModule,
        MozDynamicComponentModule
    ],
    declarations: [MozLayoutComponent],
    exports: [MozLayoutComponent],
    providers: [MozLayoutService]
})
export class MozLayoutModule {
}
