import {MozLayoutConfig_Column} from "./moz-colum-config";
import {MozLayoutConfig_Row} from "./moz-row-config";
import {L_STATES} from "./moz-base-object-config";

export class MozLayoutConfig {


    dragging:boolean = false;

    t_header: MozLayoutConfig_Row = new MozLayoutConfig_Row(L_STATES.OFF,{
        on:5,
        off:0
    });
    m_header: MozLayoutConfig_Row = new MozLayoutConfig_Row(L_STATES.ON);
    b_header: MozLayoutConfig_Row = new MozLayoutConfig_Row();
    t_footer: MozLayoutConfig_Row = new MozLayoutConfig_Row();
    m_footer: MozLayoutConfig_Row = new MozLayoutConfig_Row();
    b_footer: MozLayoutConfig_Row = new MozLayoutConfig_Row();
    l_sidebar: MozLayoutConfig_Column = new MozLayoutConfig_Column(L_STATES.ON);
    l_content: MozLayoutConfig_Column = new MozLayoutConfig_Column();
    r_content: MozLayoutConfig_Column = new MozLayoutConfig_Column();
    r_sidebar: MozLayoutConfig_Column = new MozLayoutConfig_Column();

    static getProperties() {
        return [
            't_header',
            'm_header',
            'b_header',
            't_footer',
            'm_footer',
            'b_footer',
            'l_sidebar',
            'l_content',
            'r_content',
            'r_sidebar'
        ];
    }

    static checkEntity(entity){
        if(MozLayoutConfig.getProperties().indexOf(entity) === -1){
            throw new Error("Provided entity [ '"+entity+"' ] is incorrect, available etities is " + MozLayoutConfig.getProperties().join(" | "));
        }
        return entity;
    }
}