import {L_STATES, LayoutStates, MozLayoutConfig_ObjectBase} from "./moz-base-object-config";

export class MozLayoutConfig_Row extends MozLayoutConfig_ObjectBase{
    get value(): number {
        return this._value;
    }

    set value(value: number) {
        this._value = value;
    }

    private _value:number;
    max:number = 200;
    min:number = 0;
    constructor(public state = L_STATES.OFF,public states:LayoutStates = {
        on:50,
        off:0
    }) {
        super();

        if(states[state]){
            this.value = states[state];
        }else {
            this.value = states[L_STATES.OFF];
        }
    }
}