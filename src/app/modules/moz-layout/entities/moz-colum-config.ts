import {L_STATES, LayoutStates, MozLayoutConfig_ObjectBase} from "./moz-base-object-config";

export class MozLayoutConfig_Column extends MozLayoutConfig_ObjectBase{


    get value(): number {
        return this._value;
    }

    set value(value: number) {
        this._value = value;
    }
    private _value:number;

    max:number = 400;
    min:number = 100;

    constructor(public state:string = L_STATES.OFF,public states:LayoutStates = {
        on:250,
        off:0
    }) {
        super();

        if(states.hasOwnProperty(state)){
            this.value = states[state];
        }else {
            this.value = states[L_STATES.OFF];
        }



    }
}
