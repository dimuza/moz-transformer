import {DynamicComponentItem} from "../../moz-dynamic-component/dynamic-component/dynamic-component-item";

export interface MozElements{

    t_header: DynamicComponentItem;
    m_header: DynamicComponentItem;
    b_header: DynamicComponentItem;
    t_footer: DynamicComponentItem;
    m_footer: DynamicComponentItem;
    b_footer: DynamicComponentItem;
    l_sidebar: DynamicComponentItem;
    l_content: DynamicComponentItem;
    m_content: DynamicComponentItem;

    r_content: DynamicComponentItem;
    r_sidebar: DynamicComponentItem;
}
export class MozLayoutElements{

    constructor(public elements:MozElements){

    }

}