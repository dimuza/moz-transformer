import {Moz_GSAP_Animation_Config} from "../../moz-gsap/moz-gsap-animation.class";
export interface LayoutStates{
    on:number;
    off:number;
    [key: string]: number
}
export const L_STATES = {
    ON:'on',
    OFF:'off'
};
export class MozLayoutConfig_ObjectBase{
    resizable:boolean = false;

    animation_config:Moz_GSAP_Animation_Config = new Moz_GSAP_Animation_Config();
}