import {AfterViewInit, Component, Input, OnInit} from "@angular/core";
import {MozLayoutService} from "./services/moz-layout.service";
import {MozLayoutElements} from "./entities/moz-layout-elements";

@Component({
    selector: 'app-moz-layout',
    templateUrl: './moz-layout.component.html',
    styleUrls: ['./moz-layout.component.scss']
})
export class MozLayoutComponent implements OnInit,AfterViewInit {


    @Input() elements:MozLayoutElements;
    constructor(public layout: MozLayoutService) {
    }

    ngOnInit() {


    }
    ngAfterViewInit(){
        this.layout.removedGhost();
    }



}
