import {AfterViewInit, Component, ComponentFactoryResolver, Input, OnDestroy, ViewChild} from "@angular/core";
import {DynamicComponentItem} from "./dynamic-component-item";
import {DynamicHostDirective} from "../dynamic-host.directive";


@Component({
    selector: 'app-render',
    templateUrl: './dynamic-component.component.html',
})
export class RenderComponent implements AfterViewInit {
    @Input() component: DynamicComponentItem;
    @ViewChild(DynamicHostDirective) adHost: DynamicHostDirective;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

    ngAfterViewInit() {
        this.loadComponent();
    }

    loadComponent() {

        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.component.component);

        let viewContainerRef = this.adHost.viewContainerRef;
        viewContainerRef.clear();

        let componentRef = viewContainerRef.createComponent(componentFactory);
        (componentRef.instance).data = this.component.data;
    }

}