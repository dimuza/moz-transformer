import {TweenLite,Elastic,Ease} from 'gsap';
import {Observable} from "rxjs/Observable";

export class Moz_GSAP_Animation_Config{
    animation:Ease = Elastic.easeOut;
    speed:number = 1;
}

export class Moz_GSAP_Animations{


    constructor(public currentValue:number,public endValue:number) {}

    animate(config:Moz_GSAP_Animation_Config = new Moz_GSAP_Animation_Config()){
        return new Observable(observer =>{
            let v = {
                value:this.currentValue
            };
            TweenLite.to(v,config.speed,{
                value:this.endValue,
                ease:config.animation,
                onUpdate:()=>{
                    observer.next(v.value);
                },
                onComplete:()=>{
                    observer.complete();
                }
            });
        });
    }

}