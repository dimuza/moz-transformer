import {Component, Input, OnInit} from '@angular/core';
import {MozNavProfileButtonClass} from "./moz-nav-profile-button.class";

@Component({
  selector: 'moz-nav-profile-button',
  templateUrl: './moz-nav-profile-button.component.html',
  styleUrls: ['./moz-nav-profile-button.component.scss']
})
export class MozNavProfileButtonComponent implements OnInit {

  @Input() data:MozNavProfileButtonClass;
  constructor() { }

  ngOnInit() {
  }

}
