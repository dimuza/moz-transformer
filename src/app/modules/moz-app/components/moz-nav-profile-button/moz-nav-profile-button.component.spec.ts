import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MozNavProfileButtonComponent } from './moz-nav-profile-button.component';

describe('MozNavProfileButtonComponent', () => {
  let component: MozNavProfileButtonComponent;
  let fixture: ComponentFixture<MozNavProfileButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MozNavProfileButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MozNavProfileButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
