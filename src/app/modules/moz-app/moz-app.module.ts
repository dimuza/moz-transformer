import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopMozHeaderComponent } from './layout/header/top-moz-header/top-moz-header.component';
import { MiddleMozHeaderComponent } from './layout/header/middle-moz-header/middle-moz-header.component';
import { BottomMozHeaderComponent } from './layout/header/bottom-moz-header/bottom-moz-header.component';
import { TopMozFooterComponent } from './layout/footer/top-moz-footer/top-moz-footer.component';
import { MiddleMozFooterComponent } from './layout/footer/middle-moz-footer/middle-moz-footer.component';
import { BottomMozFooterComponent } from './layout/footer/bottom-moz-footer/bottom-moz-footer.component';
import { LeftMozSidebarComponent } from './layout/sidebars/left-moz-sidebar/left-moz-sidebar.component';
import { LeftMozContentComponent } from './layout/sidebars/left-moz-content/left-moz-content.component';
import { RightMozContentComponent } from './layout/sidebars/right-moz-content/right-moz-content.component';
import { RightMozSidebarComponent } from './layout/sidebars/right-moz-sidebar/right-moz-sidebar.component';
import {MozLayoutModule} from "../moz-layout/moz-layout.module";
import { MozNavProfileButtonComponent } from './components/moz-nav-profile-button/moz-nav-profile-button.component';


@NgModule({
  imports: [
    CommonModule,
      MozLayoutModule
  ],
  declarations: [TopMozHeaderComponent, MiddleMozHeaderComponent, BottomMozHeaderComponent, TopMozFooterComponent, MiddleMozFooterComponent, BottomMozFooterComponent, LeftMozSidebarComponent, LeftMozContentComponent, RightMozContentComponent, RightMozSidebarComponent, MozNavProfileButtonComponent],
  exports: [TopMozHeaderComponent, MiddleMozHeaderComponent, BottomMozHeaderComponent, TopMozFooterComponent, MiddleMozFooterComponent, BottomMozFooterComponent, LeftMozSidebarComponent, LeftMozContentComponent, RightMozContentComponent, RightMozSidebarComponent],
  entryComponents: [TopMozHeaderComponent, MiddleMozHeaderComponent, BottomMozHeaderComponent, TopMozFooterComponent, MiddleMozFooterComponent, BottomMozFooterComponent, LeftMozSidebarComponent, LeftMozContentComponent, RightMozContentComponent, RightMozSidebarComponent],
})
export class MozAppModule { }
