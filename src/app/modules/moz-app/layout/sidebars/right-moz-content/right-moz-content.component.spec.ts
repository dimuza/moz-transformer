import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightMozContentComponent } from './right-moz-content.component';

describe('RightMozContentComponent', () => {
  let component: RightMozContentComponent;
  let fixture: ComponentFixture<RightMozContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightMozContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightMozContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
