import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftMozSidebarComponent } from './left-moz-sidebar.component';

describe('LeftMozSidebarComponent', () => {
  let component: LeftMozSidebarComponent;
  let fixture: ComponentFixture<LeftMozSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftMozSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftMozSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
