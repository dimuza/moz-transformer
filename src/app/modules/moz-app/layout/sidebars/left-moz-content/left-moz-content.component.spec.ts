import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftMozContentComponent } from './left-moz-content.component';

describe('LeftMozContentComponent', () => {
  let component: LeftMozContentComponent;
  let fixture: ComponentFixture<LeftMozContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftMozContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftMozContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
