import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightMozSidebarComponent } from './right-moz-sidebar.component';

describe('RightMozSidebarComponent', () => {
  let component: RightMozSidebarComponent;
  let fixture: ComponentFixture<RightMozSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightMozSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightMozSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
