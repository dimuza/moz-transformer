import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopMozHeaderComponent } from './top-moz-header.component';

describe('TopMozHeaderComponent', () => {
  let component: TopMozHeaderComponent;
  let fixture: ComponentFixture<TopMozHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopMozHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopMozHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
