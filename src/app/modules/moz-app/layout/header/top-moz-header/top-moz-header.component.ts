import { Component, OnInit } from '@angular/core';
import {MozLayoutService} from "../../../../moz-layout/services/moz-layout.service";

@Component({
  selector: 'app-top-moz-header',
  templateUrl: './top-moz-header.component.html',
  styleUrls: ['./top-moz-header.component.scss']
})
export class TopMozHeaderComponent implements OnInit {

  constructor(public layout:MozLayoutService) { }

  ngOnInit() {
  }

}
