import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomMozHeaderComponent } from './bottom-moz-header.component';

describe('BottomMozHeaderComponent', () => {
  let component: BottomMozHeaderComponent;
  let fixture: ComponentFixture<BottomMozHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomMozHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomMozHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
