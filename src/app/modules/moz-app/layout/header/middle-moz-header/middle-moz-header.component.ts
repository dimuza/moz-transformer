import { Component, OnInit } from '@angular/core';
import {MozLayoutService} from "../../../../moz-layout/services/moz-layout.service";
import {MozNavProfileButtonClass} from "../../../components/moz-nav-profile-button/moz-nav-profile-button.class";

@Component({
  selector: 'app-middle-moz-header',
  templateUrl: './middle-moz-header.component.html',
  styleUrls: ['./middle-moz-header.component.scss']
})
export class MiddleMozHeaderComponent implements OnInit {

  profile_button = new MozNavProfileButtonClass();
  constructor(public layout:MozLayoutService) { }

  ngOnInit() {
  }

}
