import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiddleMozHeaderComponent } from './middle-moz-header.component';

describe('MiddleMozHeaderComponent', () => {
  let component: MiddleMozHeaderComponent;
  let fixture: ComponentFixture<MiddleMozHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiddleMozHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiddleMozHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
