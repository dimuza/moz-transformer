import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomMozFooterComponent } from './bottom-moz-footer.component';

describe('BottomMozFooterComponent', () => {
  let component: BottomMozFooterComponent;
  let fixture: ComponentFixture<BottomMozFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomMozFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomMozFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
