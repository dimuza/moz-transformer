import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopMozFooterComponent } from './top-moz-footer.component';

describe('TopMozFooterComponent', () => {
  let component: TopMozFooterComponent;
  let fixture: ComponentFixture<TopMozFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopMozFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopMozFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
