import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiddleMozFooterComponent } from './middle-moz-footer.component';

describe('MiddleMozFooterComponent', () => {
  let component: MiddleMozFooterComponent;
  let fixture: ComponentFixture<MiddleMozFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiddleMozFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiddleMozFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
