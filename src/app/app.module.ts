import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {MozLayoutModule} from './modules/moz-layout/moz-layout.module';
import {MozDynamicComponentModule} from "./modules/moz-dynamic-component/moz-dynamic-component.module";
import {MozAppModule} from "./modules/moz-app/moz-app.module";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
      MozAppModule,
      MozLayoutModule,
      MozDynamicComponentModule,
      BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
