import { Component } from '@angular/core';
import {DynamicComponentItem} from "./modules/moz-dynamic-component/dynamic-component/dynamic-component-item";
import {MozElements, MozLayoutElements} from "./modules/moz-layout/entities/moz-layout-elements";
import {TopMozFooterComponent} from "./modules/moz-app/layout/footer/top-moz-footer/top-moz-footer.component";
import {MiddleMozFooterComponent} from "./modules/moz-app/layout/footer/middle-moz-footer/middle-moz-footer.component";
import {BottomMozHeaderComponent} from "./modules/moz-app/layout/header/bottom-moz-header/bottom-moz-header.component";
import {TopMozHeaderComponent} from "./modules/moz-app/layout/header/top-moz-header/top-moz-header.component";
import {MiddleMozHeaderComponent} from "./modules/moz-app/layout/header/middle-moz-header/middle-moz-header.component";
import {LeftMozSidebarComponent} from "./modules/moz-app/layout/sidebars/left-moz-sidebar/left-moz-sidebar.component";
import {LeftMozContentComponent} from "./modules/moz-app/layout/sidebars/left-moz-content/left-moz-content.component";
import {RightMozSidebarComponent} from "./modules/moz-app/layout/sidebars/right-moz-sidebar/right-moz-sidebar.component";
import {RightMozContentComponent} from "./modules/moz-app/layout/sidebars/right-moz-content/right-moz-content.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  elements:MozLayoutElements;
  constructor(){

    let elements:MozElements = {
      t_header:new DynamicComponentItem(TopMozHeaderComponent,null),
      m_header:new DynamicComponentItem(MiddleMozHeaderComponent,null),
      b_header:new DynamicComponentItem(BottomMozHeaderComponent,null),

      t_footer:new DynamicComponentItem(TopMozFooterComponent,null),
      m_footer:new DynamicComponentItem(MiddleMozHeaderComponent,null),
      b_footer:new DynamicComponentItem(BottomMozHeaderComponent,null),

      l_sidebar:new DynamicComponentItem(LeftMozSidebarComponent,null),
      l_content:new DynamicComponentItem(LeftMozContentComponent,null),

      m_content:new DynamicComponentItem(TopMozFooterComponent,null),

      r_sidebar:new DynamicComponentItem(RightMozSidebarComponent,null),
      r_content:new DynamicComponentItem(RightMozContentComponent,null)
    };
    this.elements = new MozLayoutElements(elements);
    // // let com
  }
}
